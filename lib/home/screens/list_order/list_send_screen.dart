import 'package:app_restaurant_management/home/widgets/orders/card_order.dart';
import 'package:flutter/material.dart';
import '../../../constans.dart';

class SendScreen extends StatefulWidget {
  const SendScreen({Key? key}) : super(key: key);

  @override
  _SendScreenState createState() => _SendScreenState();
}

class _SendScreenState extends State<SendScreen> {
  @override
  Widget build(BuildContext context) {
    return ListView.builder(
      padding: const EdgeInsets.all(10),
      itemCount: 10,
      itemBuilder: (context, index) {
        return const CardOrder(
          id: 'Orden #001',
          price: 'Bs. 48',
          name: 'Adela Canedo',
          labelState: 'Hace 10 min',
          colorState: greenColor,
          description: '1x Tablita',
          statusOrder: 'send',
        );
      },
    );
  }
}
